# pybind11-hello-world

This minimal Python/NumPy-based example demonstrates the use of the [pybind11](https://github.com/pybind/pybind11) binding library in concert with [scikit-build](https://github.com/scikit-build/scikit-build).

## Installation
Run `pip install --user .` to build and install locally.

## Usage

Please see 'test/test_cumsum.py' for details.

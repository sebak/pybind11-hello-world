from skbuild import setup

setup(
    name="pybind11-hello-world",
    version="1.0",
    description="pybind11/skbuild/numpy cumulative sum example package",
    author='MPCDF',
    license="MIT",
    packages=['cumsum'],
    package_dir={'': 'src'},
    cmake_install_dir='src/cumsum'
)

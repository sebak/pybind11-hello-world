// pybind11 example module '_cumsum'
#include <numeric>
#include <functional>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

// numpy-like cumulative sum, taking a NumPy array as input and returning a NumPy array
py::array_t<double> cumsum(py::array_t<double> a)
{
    // obtain information about the nd input array
    auto shape = a.request().shape;
    size_t count = std::accumulate(shape.begin(), shape.end(), 1, std::multiplies<size_t>());
    // create output NumPy array
    py::array_t<double> b(count);
    // obtain raw pointers
    double * a_p = (double*) a.request().ptr;
    double * b_p = (double*) b.request().ptr;
    // compute cumulative sum into b
    double cs = 0.0;
    for (size_t i = 0; i<count; ++i) {
        cs += a_p[i];
        b_p[i] = cs;
    }
    return b;
}

// Python binary module cumsum, exposing the cumsum C++ function to Python
PYBIND11_MODULE(_cumsum, m) {
    m.doc() = "pybind11 cumulative sum example"; // module docstring
    m.def("cumsum", &cumsum,  // third parameter is the function docstring
          "return the cumulative sum of a double-precision numpy array");
}

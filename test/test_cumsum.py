#!/usr/bin/env python3

def test_prereq():
    import numpy

def test_import():
    import cumsum

def test_cumsum():
    import numpy as np
    import cumsum
    a = np.random.rand(80)
    ap = a.reshape(20,4)
    b = cumsum.cumsum(ap)
    c = np.cumsum(ap)
    assert(np.allclose(b, c))
    print("OK!")

if __name__ == "__main__":
    test_prereq()
    test_import()
    test_cumsum()